
public class PaymentServiceFactory {

    private static PaymentServiceFactory instance = null;

    private PaymentServiceFactory() {
    }

    public static PaymentServiceFactory getInstance() {
        if (instance == null) {
            instance = new PaymentServiceFactory();
        }

        return instance;
    }

    public PaymentService getPaymentServiceType(PaymentServiceType type) {
        if (type == null) {
            return null;
        }

        if (type == PaymentServiceType.CREDIT) {
            return new CreditPaymentService();
        } else if (type == PaymentServiceType.DEBIT) {
            return new DebitPaymentService();
        }

        return null;
    }

}
