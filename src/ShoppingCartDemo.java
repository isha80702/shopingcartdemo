/*
*@author Isha
 */

public class ShoppingCartDemo {

    public static void main(String[] args) {
        PaymentServiceFactory factory = PaymentServiceFactory.getInstance();
        PaymentService creditService = factory.getPaymentServiceType(PaymentServiceType.CREDIT);
        PaymentService debitService = factory.getPaymentServiceType(PaymentServiceType.DEBIT);
        //create cart and add products
        Cart cart = new Cart();
        cart.addProduct(new Product("shirt", 50));
        cart.addProduct(new Product("pants", 60));
        //set credit service and pay
        cart.setPaymentService(creditService);
        cart.payCart();
        //set debit service and pay
        cart.setPaymentService(debitService);
        cart.payCart();
    }

}
